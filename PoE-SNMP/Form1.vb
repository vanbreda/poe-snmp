﻿Imports System
Imports SnmpSharpNet

Public Class Form1
    Dim host As String = "10.26.1.1"
    Const community As String = "public"

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtMIB.Text = "1.3.6.1.2.1.2.2.1.1"
        txtMIB.Text = "1.3.6.1.2.1.31.1.1.1"

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txtIPAddress.Text <> "" Then host = txtIPAddress.Text

        ButtonClick()
        MsgBox("Dit vraagt de hele OID op van de switch, en geeft weer waar de eerste 8 poorten '10101100' zijn")

    End Sub
    Sub ButtonClick()
        GetVersion()
        'GetContactInfo()
        'SetContactInfo()
        'GetContactInfo()
        'GetNext()
        'GetBulk()
        'Walk()
        'TestPoe()

        '' This works on HP switches : 
        'For i% = 1 To 24
        '    SetPoE(i, 1) '1 is on, 2 is off
        'Next

        Application.Exit()



        'RTF.Text = ""
        'Experiment()
        'RTF.AppendText("**KLAAR**")



        'Application.Exit()


    End Sub

    Sub Experiment()
        ' Dictionary to store values returned by GetBulk calls
        Dim result As Dictionary(Of Oid, AsnType)


        'Dim Allresult As New Dictionary(Of Oid, AsnType)
        Dim Allresult As New List(Of KeyValuePair(Of Oid, AsnType))


        ' Root Oid for the request (ifTable.ifEntry.ifDescr in this example)
        'Dim rootOid As Oid = New Oid("1.3.6.1.2.1.2.2.1.2")


        Dim rootOid As Oid = New Oid(txtMIB.Text)


        Dim nextOid As Oid = rootOid
        Dim keepGoing As Boolean = True
        ' Initialize SimpleSnmp class
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        ' Set NonRepeaters and MaxRepetitions for the GetBulk request (optional)
        snmp.NonRepeaters = 0
        snmp.MaxRepetitions = 20000

        Dim SB As New System.Text.StringBuilder
        RTF.Focus()
        While keepGoing
            ' Make a request
            result = snmp.GetBulk(New String() {nextOid.ToString()})


            RTF.AppendText(nextOid.ToString & vbCrLf)
            ' Check SNMP agent returned valid results
            If result IsNot Nothing Then
                Dim kvp As KeyValuePair(Of Oid, AsnType)
                ' Loop through returned values
                For Each kvp In result
                    ' Check that returned Oid is part of the original root Oid
                    If rootOid.IsRootOf(kvp.Key) Then
                        'Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                        '                      SnmpConstants.GetTypeName(kvp.Value.Type), _
                        '                      kvp.Value.ToString())

                        'SB.AppendLine(String.Format("{0}: ({1}) {2}", kvp.Key.ToString(), SnmpConstants.GetTypeName(kvp.Value.Type), kvp.Value.ToString()))

                        ''''''''RTF.AppendText(String.Format("{0}: ({1}) {2}", kvp.Key.ToString(), SnmpConstants.GetTypeName(kvp.Value.Type), kvp.Value.ToString()) & vbCrLf)

                        '''''''''Application.DoEvents()
                        ' Store last valid Oid to use in additional GetBulk requests (if required)
                        nextOid = kvp.Key
                        Allresult.Add(kvp)
                    Else
                        ' We found a value outside of the root Oid tree. Do not perform additional GetBulk ops
                        keepGoing = False
                    End If
                Next
            Else
                Console.WriteLine("No results received.")
                keepGoing = False
            End If
            Application.DoEvents()
        End While
        'My.Computer.FileSystem.WriteAllText("C:\vbz\MIB-4.TXT", SB.ToString, False)
        MsgBox(FindPoEOID(Allresult))

       

    End Sub

    Function FindPoEOID(R As List(Of KeyValuePair(Of Oid, AsnType))) As String
        'Dim kvp As KeyValuePair(Of Oid, AsnType)

        Dim Found As Boolean = False
        Dim i As Integer, ii As Integer
        For i = 0 To R.Count - 1
            'If R(i).Key.ToString = "1.3.6.1.2.1.105.1.1.1.3.1.1" Then Stop



            'Always begin at x.x.x.x.x.1
            'Debug.Print(R(i).Key.ToString)
            Dim _OID As String() = Split(R(i).Key.ToString, ".")

            If _OID(_OID.Count - 1) = "1" Then



                ' First thing to find is if the next 24 keys are number to 24 (meaning at least that they could be ports)

                'Make sure we don't go over bounds
                If i < R.Count - 24 Then
                    Found = True
                    For ii = 1 To 23
                        Dim oid As String() = Split(R(i + ii).Key.ToString, ".")


                        If oid(oid.Count - 1) = ii + 1 Then

                        Else
                            Found = False
                        End If
                    Next

                    If Found Then



                        If R(i).Value.ToString = R(i + 2).Value.ToString AndAlso _
                           R(i + 2).Value.ToString = R(i + 4).Value.ToString AndAlso _
                            R(i + 4).Value.ToString = R(i + 5).Value.ToString AndAlso _
                            R(i + 1).Value.ToString = R(i + 3).Value.ToString AndAlso _
                            R(i + 3).Value.ToString = R(i + 6).Value.ToString AndAlso _
                            R(i + 6).Value.ToString = R(i + 7).Value.ToString Then
                            Debug.Print("----------------------------------")

                            If R(i).Value.ToString <> R(i + 1).Value.ToString AndAlso R(i + 2).Value.ToString <> R(i + 3).Value.ToString Then
                                For x% = i To i + 24
                                    Debug.Print(R(x).Key.ToString & " :" & R(x).Value.ToString)

                                Next
                                'Stop
                                'MsgBox(R(i).Key.ToString)
                                RTF.AppendText("Possible OID: " & R(i).Key.ToString & vbCrLf)

                            End If
                        End If

                    End If


                End If

                'Debug.Print(R(i).Key.ToString)

                'Stop

            End If
        Next
        Return "Klaar"
    End Function

    Sub TestPoe()
        'MIB for poe:
        '1.3.6.1.4.1.171.12.24.2.1
        '1.3.6.1.2.1.105.1.1.1.3
        Dim requestOid() As String
        Dim result As Dictionary(Of Oid, AsnType)
        requestOid = New String() {"1.3.6.1.2.1.105.1.1.1.3"}
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        result = snmp.Get(SnmpVersion.Ver1, requestOid)
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                  SnmpConstants.GetTypeName(kvp.Value.Type), _
                                  kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("No results received.")
        End If
    End Sub



    Sub Walk()
        Dim result As Dictionary(Of Oid, AsnType)
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        result = snmp.Walk(SnmpVersion.Ver1, "1.3.6.1.2.1.1")
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                  SnmpConstants.GetTypeName(kvp.Value.Type), _
                                  kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("No results received.")
        End If
    End Sub


    Sub GetVersion()
        Dim requestOid() As String
        Dim result As Dictionary(Of Oid, AsnType)
        requestOid = New String() {"1.3.6.1.2.1.1.1.0", "1.3.6.1.2.1.1.2.0"}
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        result = snmp.Get(SnmpVersion.Ver1, requestOid)
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                  SnmpConstants.GetTypeName(kvp.Value.Type), _
                                  kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("No results received.")
        End If
    End Sub

    Sub GetContactInfo()
        Dim requestOid() As String
        Dim result As Dictionary(Of Oid, AsnType)
        requestOid = New String() {"1.3.6.1.2.1.1.4.0"}
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        result = snmp.Get(SnmpVersion.Ver1, requestOid)
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                  SnmpConstants.GetTypeName(kvp.Value.Type), _
                                  kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("No results received.")
        End If
    End Sub


    Sub SetContactInfo()

        ' Dictionary to store values returned by GetBulk calls
        Dim result As Dictionary(Of Oid, AsnType)
        ' This is the Oid of the value we will change with this example
        Dim sysContactOid As Oid = New Oid("1.3.6.1.2.1.1.4.0")
        ' The new value for the above Oid
        Dim sysContactNewValue As OctetString = New OctetString("Beheer ALLEEN door Nieuw Product")
        ' Initialize SimpleSnmp class
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        ' Create an array of Variable Bindings (Vb class) of Oid/Values you with to set
        Dim vbCollection() As Vb
        ' Variable Binding contains an Oid and value associated with that Oid
        Dim vb As Vb = New Vb(sysContactOid, sysContactNewValue)
        ' Add one or more Vb classes to the collection
        vbCollection = New Vb() {vb}
        ' Make set request
        result = snmp.Set(SnmpVersion.Ver1, vbCollection)
        ' Check SNMP agent returned valid results
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            ' Loop through returned values
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                          SnmpConstants.GetTypeName(kvp.Value.Type), _
                                          kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("Set operation failed.")
        End If
    End Sub

    Sub SetPoE(Port As Int16, Aan As Int16)

        ' Dictionary to store values returned by GetBulk calls
        Dim result As Dictionary(Of Oid, AsnType)
        ' This is the Oid of the value we will change with this example
        'This is the working PoE OID
        'Dim PoEPortOid As Oid = New Oid("1.3.6.1.2.1.105.1.1.1.3.1." & CStr(Port))

        'Test this OID - this is the found PoE OID
        Dim PoEPortOid As Oid = New Oid("1.3.6.1.2.1.2.2.1.3." & CStr(Port))


        ' The new value for the above Oid
        'Dim sysContactNewValue As OctetString = New OctetString("Beheer ALLEEN door Nieuw Product")

        Dim sysPoENewValue As SnmpSharpNet.Integer32 = New SnmpSharpNet.Integer32(Aan)
        ' Initialize SimpleSnmp class
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        ' Create an array of Variable Bindings (Vb class) of Oid/Values you with to set
        Dim vbCollection() As Vb
        ' Variable Binding contains an Oid and value associated with that Oid
        Dim vb As Vb = New Vb(PoEPortOid, sysPoENewValue)
        ' Add one or more Vb classes to the collection
        vbCollection = New Vb() {vb}
        ' Make set request
        result = snmp.Set(SnmpVersion.Ver1, vbCollection)
        ' Check SNMP agent returned valid results
        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            ' Loop through returned values
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                          SnmpConstants.GetTypeName(kvp.Value.Type), _
                                          kvp.Value.ToString())
            Next
        Else
            Console.WriteLine("Set operation failed.")
        End If
    End Sub

    Sub GetNext()
        Dim requestOid() As String
        Dim result As Dictionary(Of Oid, AsnType)
        Dim rootOid As Oid = New Oid("1.3.6.1.2.1.1")
        Dim nextOid As Oid = rootOid
        Dim keepGoing As Boolean = True
        requestOid = New String() {rootOid.ToString()}
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        While keepGoing
            result = snmp.GetNext(SnmpVersion.Ver1, New String() {nextOid.ToString()})
            If result IsNot Nothing Then
                Dim kvp As KeyValuePair(Of Oid, AsnType)
                For Each kvp In result
                    If rootOid.IsRootOf(kvp.Key) Then
                        Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                              SnmpConstants.GetTypeName(kvp.Value.Type), _
                                              kvp.Value.ToString())
                        nextOid = kvp.Key
                    Else
                        keepGoing = False
                    End If
                Next
            Else
                Console.WriteLine("No results received.")
                keepGoing = False
            End If
        End While
    End Sub

    Sub GetBulk()
        ' Dictionary to store values returned by GetBulk calls
        Dim result As Dictionary(Of Oid, AsnType)
        ' Root Oid for the request (ifTable.ifEntry.ifDescr in this example)
        Dim rootOid As Oid = New Oid("1.3.6.1.2.1.2.2.1.2")
        Dim nextOid As Oid = rootOid
        Dim keepGoing As Boolean = True
        ' Initialize SimpleSnmp class
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Exit Sub
        End If
        ' Set NonRepeaters and MaxRepetitions for the GetBulk request (optional)
        snmp.NonRepeaters = 0
        snmp.MaxRepetitions = 20
        While keepGoing
            ' Make a request
            result = snmp.GetBulk(New String() {nextOid.ToString()})
            ' Check SNMP agent returned valid results
            If result IsNot Nothing Then
                Dim kvp As KeyValuePair(Of Oid, AsnType)
                ' Loop through returned values
                For Each kvp In result
                    ' Check that returned Oid is part of the original root Oid
                    If rootOid.IsRootOf(kvp.Key) Then
                        Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                              SnmpConstants.GetTypeName(kvp.Value.Type), _
                                              kvp.Value.ToString())
                        ' Store last valid Oid to use in additional GetBulk requests (if required)
                        nextOid = kvp.Key
                    Else
                        ' We found a value outside of the root Oid tree. Do not perform additional GetBulk ops
                        keepGoing = False
                    End If
                Next
            Else
                Console.WriteLine("No results received.")
                keepGoing = False
            End If
        End While
    End Sub

    Private Sub txtMIB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMIB.KeyPress
        If e.KeyChar = Chr(13) Then
            ButtonClick()
        End If
    End Sub

    Private Sub txtMIB_TextChanged(sender As Object, e As EventArgs) Handles txtMIB.TextChanged

    End Sub
End Class
