﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Text

Public Class clsUDP
    Implements IDisposable
    Public listenClient As UdpClient
    Private listenThread As Thread
    'Public Event RawDatagram(sender As clsUDP, datagram As Byte(), ByRef handled As Boolean)
    Public Event Data_Arrival(sender As clsUDP, datagram As String) ', RemoteEndPoint As Net.IPEndPoint)


    Sub Start()
        listenThread = New Thread(New ThreadStart(AddressOf SimplestReceiver))
        listenThread.Start()
    End Sub

    Private Sub SimplestReceiver()
        Debug.Print(",,,,,,,,,,,, Overall listener thread started.")
        Dim listenEndPoint As IPEndPoint = New IPEndPoint(IPAddress.Any, 1260)
        listenClient = New UdpClient(listenEndPoint)
        Debug.Print(",,,,,,,,,,,, listen client started.")

        While True
            Debug.Print("-------listen client listening----------")
            Try
                Dim data() As Byte = listenClient.Receive(listenEndPoint)
                Dim message As String = Encoding.ASCII.GetString(data)
                If message IsNot Nothing Then Task.Factory.StartNew(Sub() RelayDatagram(message)', RemoteEndpoint))
                Debug.Print(("Listener heard: " + message))

            Catch ex As SocketException
                If (ex.ErrorCode <> 10060) Then
                    Debug.Print(ex.Message)
                Else
                    Debug.Print("expected timeout error")
                End If

            End Try

            Thread.Sleep(10)
            ' tune for your situation, can usually be omitted

        End While

    End Sub

    Private Sub RelayDatagram(data As String) ', RemoteEndPoint As Net.IPEndPoint)
        Dim handled As Boolean = False

        Try
            RaiseEvent Data_Arrival(Me, data) ', RemoteEndPoint)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    'Public Sub Dispose()
    '    CleanUp()
    'End Sub

    'Private Sub OnDisable()
    '    CleanUp()
    'End Sub

    ' be certain to catch ALL possibilities of exit in your environment,
    ' or else the thread will typically live on beyond the app quitting.
    Public Sub CleanUp()
        Debug.Print("Cleanup for listener...")
        ' note, consider carefully that it may not be running
        listenClient.Close()
        Debug.Print(",,,,, listen client correctly stopped")
        listenThread.Abort()
        listenThread.Join(5000)
        listenThread = Nothing
        Debug.Print(",,,,, listener thread correctly stopped")
    End Sub


#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
